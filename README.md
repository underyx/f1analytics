# f1analytics

Formula 1 Analytics platform

Usage:

- `git clone git@gitlab.com:chauffer/f1analytics.git`
- `docker-compose up`
- Find the port of the exposed `metabase` container
- Browse to `http://localhost:port`
- Register a local metabase account
- Have fun!

# Credits

Thanks to [ergast.com](http://ergast.com/mrd/) for the data and the MySQL database images.
